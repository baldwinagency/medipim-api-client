This library helps you connect to and make use of the Medipim API (v3 and upwards).

You aren't required to use this library to call the API, but if you are using php it should be helpful.

### Belgium ###
* API domain: https://api.medipim.be
* Medipim platform: https://platform.medipim.be
* Medipim API v4 documentation: https://platform.medipim.be/docs/api/v4/

### France ###
* API domain: https://api.medipim.fr
* Medipim platform: https://platform.medipim.fr
* Medipim API v4 documentation: https://platform.medipim.fr/docs/api/v4/

* Support: support@medipim.be

## Features ##

* Handles authentication, just provide your API key and secret.
* Takes care of encoding requests and decoding results.
* Throttles your requests and automatically retries a request should you exceed the maximum request rate.
* Neatly translates errors into exceptions.

## Installation ##

Using composer:

```
$ composer require baldwin/medipim-api-client
```

## Basic usage ##

Instantiate the client class with your API credentials. Use `get`, `post` or `stream` to make requests.

```
<?php

use Medipim\Api\Client;

require "vendor/autoload.php";

$apiKeyId = ...;
$apiKeySecret = ...;
$client = new Client($apiKeyId, $apiKeySecret, "https://api.medipim.be");

$products = $client->post("/v4/products/query", [
    "filter" => ["updatedSince" => time() - 24*60*60]
]);

foreach ($products["results"] as $product) {
    // ...
}
```

## API ##

### Client ###

#### __construct(int $apiKeyId, string $apiKeySecret, string $baseUrl)

Creates a new client.

#### get(string $path, array $query): array

Performs a GET request.

```
<?php
$r = $client->get("/v4/products/find?id=xxx");
var_dump($r); // array(2) { "product" => ... }
```

#### post(string $path, array $body): array|\Iterator

Performs a simple POST request with a json body.

Note that a few POST endpoints stream their response, in that case this function returns an `\Iterator` over the results.

```
<?php
$r = $client->post("/v4/products/query", ["filter" => ["updatedSince" => strtotime("yesterday")]]);
var_dump($r); // array(2) { ["meta"] => ..., ["results"] => ... }
```

#### stream($path, array $query = [], callable $callback = null)

This method should be used only for the /stream endpoint(s).

A callable should be defined to handle the data.

### MedipimApiError ###

extends `\RuntimeException`.

API errors are converted to an instance of this class and thrown.

You can use this object like an array to access any properties of the error.

```
<?php
try {
    $r = $client->get("/v4/foo/bar");
    // responds with {"error": {"code": "endpoint_not_found", ...}}
} catch (MedipimApiError $e) {
    var_dump($e["code"]); // string(18) "endpoint_not_found"
}
```
