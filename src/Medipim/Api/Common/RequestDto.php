<?php

namespace Medipim\Api\Common;

/**
 * @internal
 */
class RequestDto
{
    /** @var string */
    public $method;
    /** @var string */
    public $path;
    /** @var mixed|null */
    public $query;
    /** @var mixed|null */
    public $bodyJson;
    /** @var string|null */
    public $bodyFile;
    /** @var string|null */
    public $bodyFileType;
    /** @var bool */
    public $stream = false;

    public function __construct($method, $path)
    {
        $this->method = $method;
        $this->path = $path;
    }
}
